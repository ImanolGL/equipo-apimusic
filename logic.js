async function getArtist(name) {
    let urlArtist = `https://theaudiodb.com/api/v1/json/1/search.php?s=${name}`;
    console.log(urlArtist);
    let response = await fetch(urlArtist);
    let data = await response.json();
    let artists = data.artists;
    console.log(data);
    getDataArtist(artists, artist);
  }
  
  async function getAlbum() {
    let url = `https://theaudiodb.com/api/v1/json/1/mostloved.php?format=track&format=track`;
    let response = await fetch(url);
    let data = await response.json();
    let albums = data.loved;
    //console.log(albums);
    getDataAlbum(albums);
  }
  
  async function getAlbum10() {
    let url = `https://theaudiodb.com/api/v1/json/1/mostloved.php?format=album&format=album`;
    let response = await fetch(url);
    let data = await response.json();
    let albums = data.loved;
    //console.log(albums);
    getDataAlbum10(albums);
  }
  
  function getDataAlbum(albums) {
    for (let album of albums) {
      let elemento = document.querySelector(".albums50");
      let contenido = `<div class='cards'><h2 class='card'>${album.strArtist} </h2> <h3 class='card'>${album.strAlbum}</h3><p class='card'>${album.strGenre}</p></div>`;
      elemento.innerHTML += contenido;
    }
  }
  
  function getDataAlbum10(albums) {
    for (let album of albums) {
      let elemento = document.querySelector(".albums10");
      let contenido = `<div class='cards'><h2 class='card'>${album.strArtist} </h2> <h3 class='card'>${album.strAlbum}</h3><p class='card'>${album.strGenre}</p></div>`;
      elemento.innerHTML += contenido;
    }
  }