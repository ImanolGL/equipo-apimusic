async function getArtist(name) {
  let urlArtist = `https://theaudiodb.com/api/v1/json/1/search.php?s=${name}`;
  console.log(urlArtist);
  let response = await fetch(urlArtist);
  let data = await response.json();
  let artists = data.artists;
  console.log(data);
  getDataArtist(artists, artist);  
}

async function getAlbum() {
  let url = `https://theaudiodb.com/api/v1/json/1/mostloved.php?format=track&format=track`;
  let response = await fetch(url);
  let data = await response.json(); 
  let albums = data.loved;
  //console.log(albums);
  getDataAlbum(albums);
}

async function getAlbum10() {
  let url = `https://theaudiodb.com/api/v1/json/1/mostloved.php?format=album&format=album`;
  let response = await fetch(url);
  let data = await response.json();
  let albums = data.loved;
  //console.log(albums);
  getDataAlbum10(albums);
}

function getDataAlbum(albums) {
  for (let album of albums) {
    let elemento = document.querySelector(".albums50");
    let contenido = `<div class='cards'><h2 class='card'>${album.strArtist} </h2> <h3 class='card'>${album.strAlbum}</h3><p class='card'>${album.strGenre}</p></div>`;
    elemento.innerHTML += contenido;
  }
}

function getDataAlbum10(albums) {
  for (let album of albums) {
    let elemento = document.querySelector(".albums10");
    let contenido = `<div class='cards'><h2 class='card'>${album.strArtist} </h2> <h3 class='card'>${album.strAlbum}</h3><p class='card'>${album.strGenre}</p></div>`;
    elemento.innerHTML += contenido;
  }
}

function getDataArtist(dataArtist) {
  for (let artist of dataArtist) {
    document.getElementById('logo').src = artist.strArtistLogo
    document.getElementById("artist").innerHTML = artist.strArtist;
    document.getElementById("bio").innerHTML = artist.strBiographyEN;
    document.getElementById("estilo").innerHTML = artist.strStyle;
    document.getElementById("genero").innerHTML = artist.strGenre;
    document.getElementById("year").innerHTML = artist.intFormedYear;
    document.getElementById("pais").innerHTML = artist.strCountry;
    document.getElementById("website").href =
      artist.strWebsite;
    document.getElementById("twitter").href =
      artist.strTwitter;
    document.getElementById("facebook").href =
      artist.strFacebook;

  }
}

document
  .getElementById("buscadorArtista")
  .addEventListener("click", function () {
    const input = document.getElementById("input").value;
    getArtist(input);
    clearFields(input);
    if (input == '') {
      showMessage('Introduce un artista', 'danger')
    }
    blockScreenAlbums()
  });


function showMessage(message, className) {
  const div = document.createElement('div')
  div.className = `${className}`
  div.appendChild(document.createTextNode(message))
  const buscador = document.getElementById('buscador')
  const searchSongs = document.getElementById('searchSongs')
  buscador.insertBefore(div, searchSongs)

  setTimeout(() => document.querySelector('.danger').remove(), 3000)
}

document.addEventListener("DOMContentLoaded", function () {
  getAlbum();
});

document.addEventListener("DOMContentLoaded", function () {
  getAlbum10();
});

//BUSQUEDA POR ESTADO DE ANIMO
let data
async function getMood(name) {
  let urlMood = `https://theaudiodb.com/api/v1/json/1/mostloved.php?format=album&format=album`;
  let response = await fetch(urlMood);
  data = await response.json();
  //mostrarDatos(data)
  GetByMood(data.loved, name);
}

function GetByMood(getMood, strMood) {
  let results = getMood.filter(animo => animo.strMood === strMood);
  console.log(results);
  // return result
  for (result of results) {
    let eleH1 = document.createElement("h1");
    let sectionMood = document.getElementById("moodPrinted");
    eleH1.innerHTML = result.strArtist;
    sectionMood.appendChild(eleH1);
    console.log("MOODS", eleH1);

    let eleImg = document.createElement("img");
    let imageMood = document.getElementById("moodPrinted");
    eleImg.src = result.strAlbumThumb;
    imageMood.appendChild(eleImg);
    console.log("IMAGES", eleImg);

    let eleP = document.createElement("p");
    if (result.strDescriptionES === null) {
      let descriptionES = document.getElementById("moodPrinted");
      eleP.innerHTML = result.strDescription;
      descriptionES.appendChild(eleP);
    } else {
      let descriptionES = document.getElementById("moodPrinted");
      eleP.innerHTML = result.strDescriptionES;
      descriptionES.appendChild(eleP);
    }
    console.log("DESCRIPTION", eleP);
  }
}

document.getElementById("buscadorMood").addEventListener("click", function () {
  const select = document.getElementById("mood").value;
  getMood(select);
  blockScreenAlbums()
  blockScreenArtists()
  blockScreenGenres()
});

//BUSQUEDA POR GENERO

async function getGenre(name) {
  let urlGenre = `https://theaudiodb.com/api/v1/json/1/mostloved.php?format=album&format=album`;
  let response = await fetch(urlGenre);
  let data = await response.json();
  //mostrarDatos(data)
  GetByGenre(data.loved, name);
}

function GetByGenre(getGenre, strGenre) {
  let result = getGenre.filter(genero => genero.strGenre === strGenre);
  console.log(result);
  // return result
  for (result of result) {
    let eleH1 = document.createElement("h1");
    let sectionGenre = document.getElementById("genrePrinted");
    eleH1.innerHTML = result.strArtist;
    sectionGenre.appendChild(eleH1);
    console.log("GENRE", eleH1);

    let eleImg = document.createElement("img");
    let imageMood = document.getElementById("genrePrinted");
    eleImg.src = result.strAlbumThumb;
    imageMood.appendChild(eleImg);
    console.log("IMAGES", eleImg);

    let eleP = document.createElement("p");
    if (result.strDescriptionES === null) {
      let descriptionES = document.getElementById("genrePrinted");
      eleP.innerHTML = result.strDescription;
      descriptionES.appendChild(eleP);
    } else {
      let descriptionES = document.getElementById("genrePrinted");
      eleP.innerHTML = result.strDescriptionES;
      descriptionES.appendChild(eleP);
    }
    console.log("DESCRIPTION", eleP);
  }
}

function clearFields() {
  document.querySelector('#input').value = '';
}


document.getElementById("buscadorGenero").addEventListener("click", function () {
  const select = document.getElementById("genre").value;
  getGenre(select);
  blockScreenAlbums()
  blockScreenArtists()
});

/* Block Screens */

function blockScreenAlbums() {
  document.getElementById('contentAlbums50').style.display = "none"
  document.getElementById('contentAlbums10').style.display = "none"
}

function blockScreenArtists() {
  document.getElementById('datosArtista').style.display = "none"
}

function blockScreenGenres() {
  document.getElementById('genrePrinted').style.display = "none"
}

function blockScreenMoods() {
  document.getElementById('moodPrinted').style.display = "none"
}
